Put all your input files here with names `day_1.input`, etc.

If you know the expected output you can put them here to so that the
result is checked with the computed value. It should be named like
`day_23_part_2.output`.

// https://adventofcode.com/2021/day/2
// https://play.rust-lang.org/?version=stable&mode=debug&edition=2015&gist=df895b69c52a5b70bcd8f2b7f54c8ef7
pub struct Puzzle<'a>(Vec<(&'a str, i32)>);

impl<'a> Puzzle<'a> {
    pub fn new(input: &'a str) -> Result<Self, String> {
        let instructions = input.lines().map(|command| {
            command.split_once(' ')
                .ok_or(format!("Invalid command: {}", command))
                .and_then(|(action, x)| x.parse().map_or_else(
                    |e| Err(format!("Invalid X: {}: {}", x, e)),
                    |x| Ok((action, x))
                ))
        })
            .collect::<Result<_, _>>()?;
        Ok(Self(instructions))
    }
    // --- Day 2: Dive! ---
    //
    // Now, you need to figure out how to pilot this thing.
    //
    // It seems like the submarine can take a series of commands like
    // forward 1, down 2, or up 3:
    //
    //     forward X increases the horizontal position by X units.
    //     down X increases the depth by X units.
    //     up X decreases the depth by X units.
    //
    // Note that since you're on a submarine, down and up affect your
    // depth, and so they have the opposite result of what you might
    // expect.
    //
    // The submarine seems to already have a planned course (your
    // puzzle input). You should probably figure out where it's
    // going. For example:
    //
    // forward 5
    // down 5
    // forward 8
    // up 3
    // down 8
    // forward 2
    //
    // Your horizontal position and depth both start at 0. The steps
    // above would then modify them as follows:
    //
    //     forward 5 adds 5 to your horizontal position, a total of 5.
    //     down 5 adds 5 to your depth, resulting in a value of 5.
    //     forward 8 adds 8 to your horizontal position, a total of 13.
    //     up 3 decreases your depth by 3, resulting in a value of 2.
    //     down 8 adds 8 to your depth, resulting in a value of 10.
    //     forward 2 adds 2 to your horizontal position, a total of 15.
    //
    // After following these instructions, you would have a horizontal
    // position of 15 and a depth of 10. (Multiplying these together
    // produces 150.)
    //
    // Calculate the horizontal position and depth you would have
    // after following the planned course. What do you get if you
    // multiply your final horizontal position by your final depth?
    pub fn part1(&self) -> i32 {
        let (mut horizontal, mut depth) = (0, 0);
        for (action, x) in &self.0 {
            match action {
                &"forward" => horizontal += x,
                &"down" => depth += x,
                &"up" => depth -= x,
                _ => panic!()
            }
        }
        horizontal * depth
    }
    // --- Part Two ---
    //
    // Based on your calculations, the planned course doesn't seem to
    // make any sense. You find the submarine manual and discover that
    // the process is actually slightly more complicated.
    //
    // In addition to horizontal position and depth, you'll also need
    // to track a third value, aim, which also starts at 0. The
    // commands also mean something entirely different than you first
    // thought:
    //
    //     down X increases your aim by X units.
    //     up X decreases your aim by X units.
    //     forward X does two things:
    //         It increases your horizontal position by X units.
    //         It increases your depth by your aim multiplied by X.
    //
    // Again note that since you're on a submarine, down and up do the
    // opposite of what you might expect: "down" means aiming in the
    // positive direction.
    //
    // Now, the above example does something different:
    //
    //     forward 5 adds 5 to your horizontal position, a total of
    //     5. Because your aim is 0, your depth does not change.
    //
    //     down 5 adds 5 to your aim, resulting in a value of 5.
    //
    //     forward 8 adds 8 to your horizontal position, a total of
    //     13. Because your aim is 5, your depth increases by 8*5=40.
    //
    //     up 3 decreases your aim by 3, resulting in a value of 2.
    //
    //     down 8 adds 8 to your aim, resulting in a value of 10.
    //
    //     forward 2 adds 2 to your horizontal position, a total of
    //     15. Because your aim is 10, your depth increases by 2*10=20
    //     to a total of 60.
    //
    // After following these new instructions, you would have a
    // horizontal position of 15 and a depth of 60. (Multiplying these
    // produces 900.)
    //
    // Using this new interpretation of the commands, calculate the
    // horizontal position and depth you would have after following
    // the planned course. What do you get if you multiply your final
    // horizontal position by your final depth?
    pub fn part2(&self) -> i32 {
        let (mut aim, mut horizontal, mut depth) = (0, 0, 0);
        for (action, x) in &self.0 {
            match action {
                &"down" => aim += x,
                &"up" => aim -= x,
                &"forward" => {
                    horizontal += x;
                    depth += aim * x;
                }
                _ => panic!()
            }
        }
        horizontal * depth
    }
}

#[cfg(test)]
mod tests {
    use super::Puzzle;
    #[test]
    fn test_part1() {
        assert_eq!(Puzzle::new(TEST_INPUT).unwrap().part1(), 150);
    }
    #[test]
    fn test_part2() {
        assert_eq!(Puzzle::new(TEST_INPUT).unwrap().part2(), 900);
    }
    static TEST_INPUT: &str = "forward 5
down 5
forward 8
up 3
down 8
forward 2";
}

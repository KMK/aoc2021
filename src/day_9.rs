// https://adventofcode.com/2021/day/9
// https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=3f0a44fc464df542a26b056c10ad7074
use std::collections::{BinaryHeap, HashSet};
use std::cmp::Reverse;

pub struct Puzzle<'a>(Vec<&'a str>);

impl<'a> Puzzle<'a> {
    pub fn new(input: &'a str) -> Result<Self, ()> {
        Ok(Self(
            input.lines().collect()
        ))
    }
    pub fn part1(&self) -> i32 {
        let height = self.0.len();
        let width = self.0[0].len();
        let mut padded = vec![vec![u8::MAX; width + 2]; height + 2];
        for (i, row) in self.0.iter().enumerate() {
            assert_eq!(row.len(), width);
            padded[i + 1][1..width + 1].copy_from_slice(row.as_bytes());
        }

        let mut total: i32 = 0;
        for (i, row) in padded.iter().enumerate().skip(1).take(height) {
            for (j, &cell) in row.iter().enumerate().skip(1).take(width) {
                if [(-1isize, 0isize), (0, -1), (0, 1), (1, 0)].iter()
                    .all(|(di, dj)| padded[(i as isize + *di) as usize][(j as isize + *dj) as usize] > cell)
                {
                    total += i32::from(cell - b'0') + 1;
                }
            }
        }
        total
    }
    pub fn part2(&self) -> usize {
        let mut all_locations: HashSet<_> = self.0.iter().enumerate()
            .map(|(i, row)| row.bytes().enumerate()
                 .filter(|(_, cell)| *cell != b'9')
                 .map(move |(j, _)| (i, j)))
            .flatten()
            .collect();
        let mut sizes = BinaryHeap::from([Reverse(1), Reverse(1), Reverse(1)]);
        while let Some(cell) = all_locations.iter().copied().next() {
            let mut current_size = 0;
            let mut current_fresh_set = HashSet::new();
            current_fresh_set.insert(cell);
            assert!(all_locations.remove(&cell));
            while ! current_fresh_set.is_empty() {
                let seed_set = current_fresh_set;
                current_size += seed_set.len();
                current_fresh_set = HashSet::new();
                for (i, j) in seed_set {
                    [(-1isize, 0isize), (0, -1), (0, 1), (1, 0)].iter()
                        .map(|(di, dj)| ((i as isize + di) as usize, (j as isize + dj) as usize))
                        .filter_map(|neighbourhood| all_locations.take(&neighbourhood))
                        .for_each(|neighbourhood| assert!(current_fresh_set.insert(neighbourhood)));
                }
            }
            let current_min = sizes.pop().unwrap();
            sizes.push(std::cmp::min(current_min, Reverse(current_size)));
        }
        sizes.drain().map(|size| match size { Reverse(n) => n }).product()
    }
}

#[cfg(test)]
mod tests {
    use super::Puzzle;
    #[test]
    fn test_part1() {
        assert_eq!(Puzzle::new(TEST_INPUT).unwrap().part1(), 15);
    }
    #[test]
    fn test_part2() {
        assert_eq!(Puzzle::new(TEST_INPUT).unwrap().part2(), 1134);
    }
    static TEST_INPUT: &str = "2199943210
3987894921
9856789892
8767896789
9899965678
";
}

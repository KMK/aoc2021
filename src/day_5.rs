// https://adventofcode.com/2021/day/5
// https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=9a7a253f295f71a010088d3048b0d32f
use std::collections::HashMap;

pub struct Puzzle(Vec<(i32, i32, i32, i32)>);

impl Puzzle {
    pub fn new(input: &str) -> Result<Self, ()> {
        Ok(Self(
            input.lines().map(|l| {
                let p: Vec<i32> = l.split(|c| ! "-0123456789".contains(c))
                    .filter_map(|n| n.parse().ok())
                    .collect();
                assert_eq!(p.len(), 4);
                (p[0], p[1], p[2], p[3])
            }).collect()
        ))
    }
    // --- Day 5: Hydrothermal Venture ---
    //
    // You come across a field of hydrothermal vents on the ocean
    // floor! These vents constantly produce large, opaque clouds, so
    // it would be best to avoid them if possible.
    //
    // They tend to form in lines; the submarine helpfully produces a
    // list of nearby lines of vents (your puzzle input) for you to
    // review. For example:
    //
    // 0,9 -> 5,9
    // 8,0 -> 0,8
    // 9,4 -> 3,4
    // 2,2 -> 2,1
    // 7,0 -> 7,4
    // 6,4 -> 2,0
    // 0,9 -> 2,9
    // 3,4 -> 1,4
    // 0,0 -> 8,8
    // 5,5 -> 8,2
    //
    // Each line of vents is given as a line segment in the format
    // x1,y1 -> x2,y2 where x1,y1 are the coordinates of one end the
    // line segment and x2,y2 are the coordinates of the other
    // end. These line segments include the points at both ends. In
    // other words:
    //
    //     An entry like 1,1 -> 1,3 covers points 1,1, 1,2, and 1,3.
    //     An entry like 9,7 -> 7,7 covers points 9,7, 8,7, and 7,7.
    //
    // For now, only consider horizontal and vertical lines: lines
    // where either x1 = x2 or y1 = y2.
    //
    // So, the horizontal and vertical lines from the above list would
    // produce the following diagram:
    //
    // .......1..
    // ..1....1..
    // ..1....1..
    // .......1..
    // .112111211
    // ..........
    // ..........
    // ..........
    // ..........
    // 222111....
    //
    // In this diagram, the top left corner is 0,0 and the bottom
    // right corner is 9,9. Each position is shown as the number of
    // lines which cover that point or . if no line covers that
    // point. The top-left pair of 1s, for example, comes from 2,2 ->
    // 2,1; the very bottom row is formed by the overlapping lines 0,9
    // -> 5,9 and 0,9 -> 2,9.
    //
    // To avoid the most dangerous areas, you need to determine the
    // number of points where at least two lines overlap. In the above
    // example, this is anywhere in the diagram with a 2 or larger - a
    // total of 5 points.
    //
    // Consider only horizontal and vertical lines. At how many points
    // do at least two lines overlap?
    pub fn part1(&self) -> usize {
        Self::count_dangerous(
            self.0.iter().copied()
                .filter(|(x1, y1, x2, y2)| x1 == x2 || y1 == y2)
        )
    }
    // --- Part Two ---
    //
    // Unfortunately, considering only horizontal and vertical lines
    // doesn't give you the full picture; you need to also consider
    // diagonal lines.
    //
    // Because of the limits of the hydrothermal vent mapping system,
    // the lines in your list will only ever be horizontal, vertical,
    // or a diagonal line at exactly 45 degrees. In other words:
    //
    //     An entry like 1,1 -> 3,3 covers points 1,1, 2,2, and 3,3.
    //     An entry like 9,7 -> 7,9 covers points 9,7, 8,8, and 7,9.
    //
    // Considering all lines from the above example would now produce
    // the following diagram:
    //
    // 1.1....11.
    // .111...2..
    // ..2.1.111.
    // ...1.2.2..
    // .112313211
    // ...1.2....
    // ..1...1...
    // .1.....1..
    // 1.......1.
    // 222111....
    //
    // You still need to determine the number of points where at least
    // two lines overlap. In the above example, this is still anywhere
    // in the diagram with a 2 or larger - now a total of 12 points.
    //
    // Consider all of the lines. At how many points do at least two
    // lines overlap?
    pub fn part2(&self) -> usize {
        Self::count_dangerous(self.0.iter().copied())
    }
    fn count_dangerous<I: Iterator<Item=(i32, i32, i32, i32)>>(i: I) -> usize {
        let mut points = HashMap::new();
        for (x1, y1, x2, y2) in i {
            let dx = (x2 - x1).signum();
            let dy = (y2 - y1).signum();
            let steps = if dx == 0 { y2 - y1 } else { x2 - x1 };
            for i in 0..steps.abs() + 1 {
                let p = (x1 + i * dx, y1 + i * dy);
                points.entry(p).and_modify(|v| *v += 1).or_insert(1);
            }
        }
        points.values().filter(|v| **v > 1).count()
    }
}

#[cfg(test)]
mod tests {
    use super::Puzzle;
    #[test]
    fn test_part1() {
        assert_eq!(Puzzle::new(TEST_INPUT).unwrap().part1(), 5);
    }
    #[test]
    fn test_part2() {
        assert_eq!(Puzzle::new(TEST_INPUT).unwrap().part2(), 12);
    }
    static TEST_INPUT: &str = "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
";
}

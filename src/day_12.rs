// https://adventofcode.com/2021/day/12
use std::collections::HashMap;

pub struct Puzzle<'a>(Vec<(&'a str, &'a str)>);

impl<'a> Puzzle<'a> {
    pub fn new(input: &'a str) -> Result<Self, ()> {
        let paths: Vec<_> = input.lines()
            .map(|l| l.split_once('-').unwrap())
            .collect();
        Ok(Self(paths))
    }
    pub fn part1(&self) -> i32 {
        // (name, Vec<other-cave>, visited)
        let mut maze: Vec<(&str, Vec<usize>, bool)> = Vec::new();
        let mut caves: HashMap<&str, usize> = HashMap::new();
        fn intern<'a>(
            maze: &mut Vec<(&'a str, Vec<usize>, bool)>,
            caves: &mut HashMap<&'a str, usize>,
            name: &'a str
        ) -> usize {
            *caves.entry(name)
                .or_insert_with(|| {
                    let i = maze.len();
                    maze.push((name, Vec::new(), false));
                    i
                })
        }
        for (a, b) in &self.0 {
            let a = intern(&mut maze, &mut caves, a);
            let b = intern(&mut maze, &mut caves, b);
            assert!(! maze[a].1.contains(&b));
            assert!(! maze[b].1.contains(&a));
            maze[a].1.push(b);
            maze[b].1.push(a);
        }

        let mut count = 0;
        maze[caves["start"]].2 = true;
        // [(cave, next_idx_other_cave_to_visit)]
        let mut path = vec![(caves["start"], 0)];
        loop {
            let (cave, next_idx_other_cave) = match path.last_mut() {
                None => break,
                Some(location) => location
            };
            let idx_other_cave = maze[*cave].1.iter().copied().enumerate()
                .skip(*next_idx_other_cave)
                .find(|(_, i)| maze[*i].0.as_bytes()[0].is_ascii_uppercase()
                      || ! maze[*i].2);
            match idx_other_cave {
                None => match path.pop() {
                    None => break,
                    Some((cave, _)) => maze[cave].2 = false
                }
                Some((num_path, i)) => {
                    *next_idx_other_cave = num_path + 1;
                    if maze[i].0 == "end" {
                        count += 1;
                    } else {
                        let other_cave = &mut maze[i];
                        other_cave.2 = true;
                        path.push((i, 0));
                    }
                }
            }
        }

        count
    }
    pub fn part2(&self) -> i32 {
        // (name, Vec<other-cave>, visited_count)
        let mut maze: Vec<(&str, Vec<usize>, usize)> = Vec::new();
        let mut caves: HashMap<&str, usize> = HashMap::new();
        fn intern<'a>(
            maze: &mut Vec<(&'a str, Vec<usize>, usize)>,
            caves: &mut HashMap<&'a str, usize>,
            name: &'a str
        ) -> usize {
            *caves.entry(name)
                .or_insert_with(|| {
                    let i = maze.len();
                    maze.push((name, Vec::new(), 0));
                    i
                })
        }
        for (a, b) in &self.0 {
            let a = intern(&mut maze, &mut caves, a);
            let b = intern(&mut maze, &mut caves, b);
            assert!(! maze[a].1.contains(&b));
            assert!(! maze[b].1.contains(&a));
            maze[a].1.push(b);
            maze[b].1.push(a);
        }

        let mut count = 0;
        let mut did_extra_visit = false;
        maze[caves["start"]].2 = 2; // Make sure we don't visit it more
        // [(cave, next_idx_other_cave_to_visit)]
        let mut path = vec![(caves["start"], 0)];
        loop {
            let (cave, next_idx_other_cave) = match path.last_mut() {
                None => break,
                Some(location) => location
            };
            let idx_other_cave = maze[*cave].1.iter().copied().enumerate()
                .skip(*next_idx_other_cave)
                .find(|(_, i)| maze[*i].2 == 0
                      ||  ! did_extra_visit && maze[*i].2 == 1
                      || maze[*i].0.as_bytes()[0].is_ascii_uppercase());
            match idx_other_cave {
                None => match path.pop() {
                    None => break,
                    Some((cave, _)) => {
                        maze[cave].2 -= 1;
                        if maze[cave].2 == 1 && maze[cave].0.as_bytes()[0].is_ascii_lowercase() && maze[cave].0 != "start" {
                            assert!(did_extra_visit);
                            did_extra_visit = false;
                        }
                    }
                }
                Some((num_path, i)) => {
                    *next_idx_other_cave = num_path + 1;
                    if maze[i].0 == "end" {
                        count += 1;
                    } else {
                        let other_cave = &mut maze[i];
                        did_extra_visit |= other_cave.2 > 0 && other_cave.0.as_bytes()[0].is_ascii_lowercase();
                        other_cave.2 += 1;
                        path.push((i, 0));
                    }
                }
            }
        }

        count
    }
}

#[cfg(test)]
mod tests {
    use super::Puzzle;
    #[test]
    fn test_part1() {
        assert_eq!(Puzzle::new(TEST_INPUT_1).unwrap().part1(), 10);
        assert_eq!(Puzzle::new(TEST_INPUT_2).unwrap().part1(), 19);
        assert_eq!(Puzzle::new(TEST_INPUT_3).unwrap().part1(), 226);
    }
    #[test]
    fn test_part2() {
        assert_eq!(Puzzle::new(TEST_INPUT_1).unwrap().part2(), 36);
        assert_eq!(Puzzle::new(TEST_INPUT_2).unwrap().part2(), 103);
        assert_eq!(Puzzle::new(TEST_INPUT_3).unwrap().part2(), 3509);
    }
    static TEST_INPUT_1: &str = "start-A
start-b
A-c
A-b
b-d
A-end
b-end
";
    static TEST_INPUT_2: &str = "dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc
";
    static TEST_INPUT_3: &str = "fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW
";
}

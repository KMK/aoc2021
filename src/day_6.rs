// https://adventofcode.com/2021/day/6
// https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=31a8824a34ac76cca45cd493016eca24

pub fn new(input: &str) -> Result<Puzzle<7, 9>, impl std::error::Error> {
    Puzzle::new(input)
}

// LEN is the number of days before giving birth. TOT_LEN is LEN plus
// the number of extra days newborn wait before giving birth.
pub struct Puzzle<const LEN: usize, const TOT_LEN: usize>([usize; LEN]);

impl<const LEN: usize, const TOT_LEN: usize> Puzzle<LEN, TOT_LEN> {
    pub fn new(input: &str) -> Result<Self, std::num::ParseIntError> {
        let mut puzzle = Self([0; LEN]);
        for c in input.lines().map(|l| l.split(',')).flatten() {
            puzzle.0[c.parse::<usize>()?] += 1;
        }
        Ok(puzzle)
    }
    // --- Day 6: Lanternfish ---
    //
    // The sea floor is getting steeper. Maybe the sleigh keys got
    // carried this way?
    //
    // A massive school of glowing lanternfish swims past. They must
    // spawn quickly to reach such large numbers - maybe exponentially
    // quickly? You should model their growth rate to be sure.
    //
    // Although you know nothing about this specific species of
    // lanternfish, you make some guesses about their
    // attributes. Surely, each lanternfish creates a new lanternfish
    // once every 7 days.
    //
    // However, this process isn't necessarily synchronized between
    // every lanternfish - one lanternfish might have 2 days left
    // until it creates another lanternfish, while another might have
    // 4. So, you can model each fish as a single number that
    // represents the number of days until it creates a new
    // lanternfish.
    //
    // Furthermore, you reason, a new lanternfish would surely need
    // slightly longer before it's capable of producing more
    // lanternfish: two more days for its first cycle.
    //
    // So, suppose you have a lanternfish with an internal timer value of 3:
    //
    //     After one day, its internal timer would become 2.
    //
    //     After another day, its internal timer would become 1.
    //
    //     After another day, its internal timer would become 0.
    //
    //     After another day, its internal timer would reset to 6, and
    //     it would create a new lanternfish with an internal timer of
    //     8.
    //
    //     After another day, the first lanternfish would have an
    //     internal timer of 5, and the second lanternfish would have
    //     an internal timer of 7.
    //
    // A lanternfish that creates a new fish resets its timer to 6,
    // not 7 (because 0 is included as a valid timer value). The new
    // lanternfish starts with an internal timer of 8 and does not
    // start counting down until the next day.
    //
    // Realizing what you're trying to do, the submarine automatically
    // produces a list of the ages of several hundred nearby
    // lanternfish (your puzzle input). For example, suppose you were
    // given the following list:
    //
    // 3,4,3,1,2
    //
    // This list means that the first fish has an internal timer of 3,
    // the second fish has an internal timer of 4, and so on until the
    // fifth fish, which has an internal timer of 2. Simulating these
    // fish over several days would proceed as follows:
    //
    // Initial state: 3,4,3,1,2
    // After  1 day:  2,3,2,0,1
    // After  2 days: 1,2,1,6,0,8
    // After  3 days: 0,1,0,5,6,7,8
    // After  4 days: 6,0,6,4,5,6,7,8,8
    // After  5 days: 5,6,5,3,4,5,6,7,7,8
    // After  6 days: 4,5,4,2,3,4,5,6,6,7
    // After  7 days: 3,4,3,1,2,3,4,5,5,6
    // After  8 days: 2,3,2,0,1,2,3,4,4,5
    // After  9 days: 1,2,1,6,0,1,2,3,3,4,8
    // After 10 days: 0,1,0,5,6,0,1,2,2,3,7,8
    // After 11 days: 6,0,6,4,5,6,0,1,1,2,6,7,8,8,8
    // After 12 days: 5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8
    // After 13 days: 4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8
    // After 14 days: 3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8
    // After 15 days: 2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7
    // After 16 days: 1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8
    // After 17 days: 0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8
    // After 18 days: 6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8
    //
    // Each day, a 0 becomes a 6 and adds a new 8 to the end of the
    // list, while each other number decreases by 1 if it was present
    // at the start of the day.
    //
    // In this example, after 18 days, there are a total of 26
    // fish. After 80 days, there would be a total of 5934.
    //
    // Find a way to simulate lanternfish. How many lanternfish would
    // there be after 80 days?
    pub fn part1(&self) -> usize {
        self.solve(80)
    }
    // --- Part Two ---
    //
    // Suppose the lanternfish live forever and have unlimited food
    // and space. Would they take over the entire ocean?
    //
    // After 256 days in the example above, there would be a total of
    // 26984457539 lanternfish!
    //
    // How many lanternfish would there be after 256 days?
    pub fn part2(&self) -> usize {
        self.solve(256)
    }
    // The cyclic array is:
    //
    // [ counter=0,     counter=1, ..., counter=6, nursery day 2, nursery day 1]
    //
    // and when the day index `d` advances, this automatically
    // "shifts" the counters and nursery days into the right
    // place. There still need to put the new baby fishes into nursery
    // day 2 (that becomes counter=6 when the day is next shifted)..
    //
    // [ nursery day 1, counter=0, counter=1, ..., counter=6,     nursery day 2]
    //
    fn solve(&self, days: usize) -> usize {
        let mut cycle = [0usize; TOT_LEN];
        cycle[0..self.0.len()].copy_from_slice(&self.0);
        for d in 0..days {
            let nursery_2 = (d + self.0.len()) % cycle.len();
            let counter_0 = d % cycle.len();
            cycle[nursery_2] += cycle[counter_0];
        }
        cycle.into_iter().sum()
    }
}

#[cfg(test)]
mod tests {
    use super::new;
    #[test]
    fn test_solve1() {
        let puzzle = new(TEST_INPUT).unwrap();
        assert_eq!(puzzle.solve(0), 5);
        assert_eq!(puzzle.solve(1), 5);
        assert_eq!(puzzle.solve(2), 6);
        assert_eq!(puzzle.solve(3), 7);
        assert_eq!(puzzle.solve(4), 9);
        assert_eq!(puzzle.solve(5), 10);
        assert_eq!(puzzle.solve(6), 10);
        assert_eq!(puzzle.solve(7), 10);
        assert_eq!(puzzle.solve(8), 10);
        assert_eq!(puzzle.solve(9), 11);
        assert_eq!(puzzle.solve(10), 12);
        assert_eq!(puzzle.solve(11), 15);
        assert_eq!(puzzle.solve(12), 17);
        assert_eq!(puzzle.solve(13), 19);
        assert_eq!(puzzle.solve(14), 20);
    }
    #[test]
    fn test_part1() {
        assert_eq!(new(TEST_INPUT).unwrap().part1(), 5934);
    }
    #[test]
    fn test_part2() {
        assert_eq!(new(TEST_INPUT).unwrap().part2(), 26984457539);
    }
    static TEST_INPUT: &str = "3,4,3,1,2";
}

// https://adventofcode.com/2021/day/4
// https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=3a7acb430b7ae3e09e24863e3b3684fc
pub struct Puzzle {
    numbers: Vec<i32>,
    boards: Vec<Vec<i32>>,
    rows: usize,
    columns: usize
}

impl Puzzle {
    pub fn new(input: &str) -> Result<Self, ()> {
        let mut lines = input.lines();
        let l = lines.next().unwrap();
        let numbers: Vec<i32> = l.split(',')
            .map(|s| s.parse().unwrap())
            .collect();
        assert_eq!(lines.next(), Some(""));

        let mut boards = Vec::new();
        let mut rows = 0;
        let mut columns = 0;
        let mut cur_board = Vec::new();
        let mut cur_rows = 0;
        let mut cur_columns = 0;
        loop {
            match lines.next() {
                next @ None | next @ Some("") => {
                    if rows == 0 {
                        rows = cur_rows;
                        columns = cur_columns;
                    }
                    assert_eq!((rows, columns), (cur_rows, cur_columns));
                    boards.push(cur_board);
                    if next.is_none() {
                        return Ok(Self { numbers, boards, rows, columns });
                    }
                    cur_board = Vec::new();
                    cur_rows = 0;
                    cur_columns = 0;
                }
                Some(s) => {
                    let r = s.split(' ')
                        .filter(|cell| ! cell.is_empty()) // skip padding whites
                        .map(|cell| cell.parse::<i32>().unwrap());
                    cur_board.extend(r);
                    cur_rows += 1;
                    match cur_rows {
                        // The first added row tells us the number of columns
                        1 => cur_columns = cur_board.len(),
                        _ => assert!(cur_columns * cur_rows == cur_board.len())
                    }
                }
            }
        }
    }
    // --- Day 4: Giant Squid ---
    //
    // You're already almost 1.5km (almost a mile) below the surface
    // of the ocean, already so deep that you can't see any
    // sunlight. What you can see, however, is a giant squid that has
    // attached itself to the outside of your submarine.
    //
    // Maybe it wants to play bingo?
    //
    // Bingo is played on a set of boards each consisting of a 5x5
    // grid of numbers. Numbers are chosen at random, and the chosen
    // number is marked on all boards on which it appears. (Numbers
    // may not appear on all boards.) If all numbers in any row or any
    // column of a board are marked, that board wins. (Diagonals don't
    // count.)
    //
    // The submarine has a bingo subsystem to help passengers
    // (currently, you and the giant squid) pass the time. It
    // automatically generates a random order in which to draw numbers
    // and a random set of boards (your puzzle input). For example:
    //
    // 7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1
    //
    // 22 13 17 11  0
    //  8  2 23  4 24
    // 21  9 14 16  7
    //  6 10  3 18  5
    //  1 12 20 15 19
    //
    //  3 15  0  2 22
    //  9 18 13 17  5
    // 19  8  7 25 23
    // 20 11 10 24  4
    // 14 21 16 12  6
    //
    // 14 21 17 24  4
    // 10 16 15  9 19
    // 18  8 23 26 20
    // 22 11 13  6  5
    //  2  0 12  3  7
    //
    // After the first five numbers are drawn (7, 4, 9, 5, and 11),
    // there are no winners, but the boards are marked as follows
    // (shown here adjacent to each other to save space):
    //
    // 22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
    //  8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
    // 21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
    //  6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
    //  1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
    //
    // After the next six numbers are drawn (17, 23, 2, 0, 14, and
    // 21), there are still no winners:
    //
    // 22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
    //  8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
    // 21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
    //  6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
    //  1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
    //
    // Finally, 24 is drawn:
    //
    // 22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
    //  8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
    // 21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
    //  6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
    //  1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
    //
    // At this point, the third board wins because it has at least one
    // complete row or column of marked numbers (in this case, the
    // entire top row is marked: 14 21 17 24 4).
    //
    // The score of the winning board can now be calculated. Start by
    // finding the sum of all unmarked numbers on that board; in this
    // case, the sum is 188. Then, multiply that sum by the number
    // that was just called when the board won, 24, to get the final
    // score, 188 * 24 = 4512.
    //
    // To guarantee victory against the giant squid, figure out which
    // board will win first. What will your final score be if you
    // choose that board?
    pub fn part1(&self) -> i32 {
        let (winner, draws) = self.boards.iter()
            .filter_map(|brd| self.evaluate(brd).map(|drws| (brd, drws)))
            .min_by_key(|(_, streak)| streak.len())
            .unwrap();
        Self::score(winner, draws)
    }
    // --- Part Two ---
    //
    // On the other hand, it might be wise to try a different
    // strategy: let the giant squid win.
    //
    // You aren't sure how many bingo boards a giant squid could play
    // at once, so rather than waste time counting its arms, the safe
    // thing to do is to figure out which board will win last and
    // choose that one. That way, no matter which boards it picks, it
    // will win for sure.
    //
    // In the above example, the second board is the last to win,
    // which happens after 13 is eventually called and its middle
    // column is completely marked. If you were to keep playing until
    // this point, the second board would have a sum of unmarked
    // numbers equal to 148 for a final score of 148 * 13 = 1924.
    //
    // Figure out which board will win last. Once it wins, what would
    // its final score be?
    pub fn part2(&self) -> i32 {
        let (winner, draws) = self.boards.iter()
            .filter_map(|brd| self.evaluate(brd).map(|drws| (brd, drws)))
            .max_by_key(|(_, streak)| streak.len())
            .unwrap();
        Self::score(winner, draws)
    }
    // See if this board can be finished and return the needed draws
    fn evaluate(&self, board: &[i32]) -> Option<&[i32]> {
        let mut rcounts = vec![self.columns; self.rows].into_boxed_slice();
        let mut ccounts = vec![self.rows; self.columns].into_boxed_slice();
        for (i, draw) in self.numbers.iter().enumerate() {
            if self.numbers[0..i].contains(draw) {
                continue;       // duplicate draw
            }
            if let Some(x) = board.iter().position(|y| y == draw) {
                let row = x / self.columns;
                let column = x % self.columns;
                rcounts[row] -= 1;
                ccounts[column] -= 1;
                if rcounts[row] == 0 || ccounts[column] == 0 {
                    return Some(&self.numbers[0..i+1]);
                }
            }
        }
        None
    }
    fn score(board: &[i32], draws: &[i32]) -> i32 {
        let sum: i32 = board.iter().filter(|n| ! draws.contains(n)).sum();
        sum * draws.last().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::Puzzle;
    #[test]
    fn test_evaluate() {
        let puzzle = Puzzle::new(TEST_INPUT).unwrap();
        assert_eq!(
            puzzle.evaluate(&puzzle.boards[0]),
            Some(&[7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16][..])
        );
        assert_eq!(
            puzzle.evaluate(&puzzle.boards[1]),
            Some(&[7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13][..])
        );
        assert_eq!(
            puzzle.evaluate(&puzzle.boards[2]),
            Some(&[7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24][..])
        );
    }
    #[test]
    fn test_part1() {
        assert_eq!(Puzzle::new(TEST_INPUT).unwrap().part1(), 4512);
    }
    #[test]
    fn test_part2() {
        assert_eq!(Puzzle::new(TEST_INPUT).unwrap().part2(), 1924);
    }
    static TEST_INPUT: &str = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
";
}

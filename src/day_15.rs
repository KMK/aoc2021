// https://adventofcode.com/2021/day/15
use std::collections::BTreeSet;
pub struct Puzzle(Vec<Vec<u8>>);

impl Puzzle {
    pub fn new(input: &str) -> Result<Self, ()> {
        let grid = input.lines().map(|l|{
            l.bytes().map(|b| b - b'0').collect()
        }).collect();
        Ok(Self(grid))
    }
    pub fn part1(&self) -> i32 {
        let height = self.0.len();
        let width = self.0[0].len();
        let mut risk = vec![vec![i32::MAX; width]; height];
        let mut explore = BTreeSet::new();
        risk[0][0] = 0;
        explore.insert((0usize, 0usize));
        while let Some(cell) = explore.iter().next() {
            let cell = *cell;
            explore.remove(&cell);
            for (dx, dy) in [(0isize, -1isize), (-1, 0), (1, 0), (0, 1)] {
                let nx = cell.0 as isize + dx;
                let ny = cell.1 as isize + dy;
                if 0 <= nx && nx < width as isize && 0 <= ny && ny < height as isize {
                    let nx = nx as usize;
                    let ny = ny as usize;
                    let r = risk[cell.1][cell.0] + i32::from(self.0[ny][nx]);
                    if r < risk[ny][nx] {
                        risk[ny][nx] = r;
                        explore.insert((nx, ny));
                    }
                }
            }
        }
        risk[height - 1][width - 1]
    }
    pub fn part2(&self) -> i32 {
        let mut grid = vec![];
        for downward in 0..5 {
            for line in self.0.iter() {
                let mut l = vec![];
                for rightward in 0..5 {
                    for r in line.iter().copied() {
                        l.push((r + downward + rightward - 1) % 9 + 1);
                    }
                }
                grid.push(l);
            }
        }
        Self(grid).part1()
    }
}

#[cfg(test)]
mod tests {
    use super::Puzzle;
    #[test]
    fn test_part1() {
        assert_eq!(Puzzle::new(TEST_INPUT).unwrap().part1(), 40);
    }
    #[test]
    fn test_part2() {
        assert_eq!(Puzzle::new(TEST_INPUT).unwrap().part2(), 315);
    }
    static TEST_INPUT: &str = "1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581
";
}

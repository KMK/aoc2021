fn check_answer<S: AsRef<str> >(answer: S, name: &str) -> Option<String> {
    let ans = answer.as_ref();
    print!("{:>13}", ans);
    let path: std::path::PathBuf = [
        env!("CARGO_MANIFEST_DIR"),
        "input",
        name
    ].into_iter().collect();
    let ans = format!("{}", ans);
    match std::fs::read_to_string(&path) {
        Err(e) => {
            print!(" ?");
            Some(format!("{:?}: {}", &path, e))
        }
        Ok(s) if ans.trim() != s.trim() => {
            print!(" ?");
            Some(format!("Left: {:?}, Right: {:?}", ans.trim(), s.trim()))
        }
        Ok(_) => None
    }
}

macro_rules! day (
    ( $new:path ) => {
        let new = stringify!($new);
        let name = new.split_once(':').map(|(name, _)| name).unwrap_or(new);
        print!("{:<6} ", name);
        let path: std::path::PathBuf = [
            env!("CARGO_MANIFEST_DIR"),
            "input",
            &format!("{}.input", name)
        ].into_iter().collect();
        match std::fs::read_to_string(&path) {
            Err(err) => {
                println!("?");
                println!("\t{:?}: {}", &path, err)
            }
            Ok(input) => {
                let day = $new(input.as_str()).unwrap();

                let answer_1 = day.part1().to_string();
                let part_1_name = format!("{}_part_1.output", name);
                let err_1 = check_answer(&answer_1, &part_1_name);
                print!("\t");

                let answer_2 = day.part2().to_string();
                let part_2_name = format!("{}_part_2.output", name);
                let err_2 = check_answer(&answer_2, &part_2_name);
                println!();

                for e in [err_1, err_2].into_iter().flatten() {
                    println!("\t{}", e);
                }
            }
        }
    };
);

fn main() {
    day!(day_1::Puzzle::new);
    day!(day_2::Puzzle::new);
    day!(day_3::Puzzle::new);
    day!(day_4::Puzzle::new);
    day!(day_5::Puzzle::new);
    day!(day_6::new);
    day!(day_7::Puzzle::new);
    day!(day_8::Puzzle::new);
    day!(day_9::Puzzle::new);
    day!(day_10::Puzzle::new);
    day!(day_11::Puzzle::new);
    day!(day_12::Puzzle::new);
    day!(day_13::Puzzle::new);
    day!(day_14::Puzzle::new);
    day!(day_15::Puzzle::new);
}

mod day_1;
mod day_2;
mod day_3;
mod day_4;
mod day_5;
mod day_6;
mod day_7;
mod day_8;
mod day_9;
mod day_10;
mod day_11;
mod day_12;
mod day_13;
mod day_14;
mod day_15;

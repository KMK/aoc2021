// https://adventofcode.com/2021/day/12
use std::collections::HashSet;
use std::fmt::Write;

pub struct Puzzle(Vec<(i32, i32)>, Vec<(u8, i32)>);

impl Puzzle {
    pub fn new(input: &str) -> Result<Self, ()> {
        let mut dots = Vec::new();
        let mut folds = Vec::new();
        for l in input.lines() {
            if l == "" {
            } else if let Some(f) = l.strip_prefix("fold along ") {
                let (axis, value) = f.split_once('=').unwrap();
                let axis = axis.as_bytes()[0];
                let value = value.parse().unwrap();
                folds.push((axis, value));
            } else {
                let (x, y) = l.split_once(',').unwrap();
                let x = x.parse().unwrap();
                let y = y.parse().unwrap();
                dots.push((x, y));
            }
        }
        Ok(Self(dots, folds))
    }
    pub fn part1(&self) -> i32 {
        let mut sheet: HashSet<_> = self.0.iter().copied().collect();
        for (axis, value) in self.1.iter().copied().take(1) {
            let mut folded = HashSet::new();
            for (x, y) in sheet {
                let pair = match axis {
                    b'x' => (value - (value - x).abs(), y),
                    b'y' => (x, value - (value - y).abs()),
                    _ => panic!()
                };
                folded.insert(pair);
            }
            sheet = folded;
        }
        let res = sheet.len() as i32;
        assert_ne!(res, 1683);
        assert_ne!(res, 847);
        res
    }
    pub fn part2(&self) -> String {
        let mut sheet: HashSet<_> = self.0.iter().copied().collect();
        for (axis, value) in self.1.iter().copied() {
            let mut folded = HashSet::new();
            for (x, y) in sheet {
                let pair = match axis {
                    b'x' => (value - (value - x).abs(), y),
                    b'y' => (x, value - (value - y).abs()),
                    _ => panic!()
                };
                folded.insert(pair);
            }
            sheet = folded;
        }
        let width = sheet.iter().map(|(x, _)| *x).max().unwrap() + 1;
        let height = sheet.iter().map(|(_, y)| *y).max().unwrap() + 1;
        let mut res = String::new();
        for y in 0..height {
            for x in 0..width {
                write!(&mut res, "{}", if sheet.contains(&(x, y)) { '#' } else { '.' });
            }
            writeln!(&mut res);
        }
        res
    }
}

#[cfg(test)]
mod tests {
    use super::Puzzle;
    #[test]
    fn test_part1() {
        assert_eq!(Puzzle::new(TEST_INPUT).unwrap().part1(), 17);
    }
    #[test]
    fn test_part2() {
        assert_eq!(
            Puzzle::new(TEST_INPUT).unwrap().part2(),
            "#####
#...#
#...#
#...#
#####
"
        );
    }
    static TEST_INPUT: &str = "6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5
";
}

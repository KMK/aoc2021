use std::collections::HashMap;
pub struct Puzzle<'a>(&'a str, HashMap<(u8, u8), u8>);

impl<'a> Puzzle<'a> {
    pub fn new(input: &'a str) -> Result<Self, ()> {
        let mut lines = input.lines();
        let template = lines.next().unwrap();
        assert_eq!(lines.next(), Some(""));
        let rules = lines.map(|l| {
            let (pair, insertion) = l.split_once(" -> ").unwrap();
            let pair = pair.as_bytes();
            let insertion = insertion.as_bytes();
            ((pair[0], pair[1]), insertion[0])
        })
            .collect();
        Ok(Self(template, rules))
    }
    pub fn part1(&self) -> u64 {
        let mut polymer = self.0.as_bytes().to_vec();
        for _ in 0..10 {
            let mut reacted = Vec::new();
            for pair in polymer.windows(2) {
                reacted.push(pair[0]);
                if let Some(b) = self.1.get(&(pair[0], pair[1])) {
                    reacted.push(*b);
                }
            }
            if let Some(b) = polymer.last() {
                reacted.push(*b);
            }
            polymer = reacted;
        }
        let mut freq = [0; 256];
        for b in polymer {
            freq[usize::from(b)] += 1;
        }
        let min = freq.iter().filter(|&&n| n > 0).min().unwrap();
        let max = freq.iter().max().unwrap();
        max - min
    }
    pub fn part2(&self) -> u64 {
        let mut cache = HashMap::new();
        let mut freq = self.solve(&mut cache, self.0.as_bytes(), 40);
        freq[(self.0.as_bytes().last().unwrap() - b'A') as usize] += 1;
        let min = freq.iter().filter(|&&n| n > 0).min().unwrap();
        let max = freq.iter().max().unwrap();
        max - min
    }
    // Note: cache entries *exclude* counting the last letter
    fn solve(
        &self,
        cache: &mut HashMap<(u8, u8, usize), [u64; 26]>,
        template: &[u8],
        count: usize,
    ) -> [u64; 26] {
        fn freq_idx(letter: u8) -> usize {
            usize::from(letter.checked_sub(b'A').unwrap())
        }
        let mut res = [0; 26];
        if template.is_empty() {
            return res;
        }
        if count == 0 || template.len() == 1 {
            for letter in &template[..template.len() - 1] {
                res[freq_idx(*letter)] += 1;
            }
            return res;
        }
        for w in template.windows(2) {
            let k = (w[0], w[1], count);
            let freq = match cache.get(&k) {
                Some(freq) => freq,
                None => {
                    let freq = if let Some(insert) = self.1.get(&(w[0], w[1])) {
                        self.solve(cache, &[w[0], *insert, w[1]][..], count - 1)
                    } else {
                        let mut freq = [0; 26];
                        freq[freq_idx(w[0])] = 1;
                        freq
                    };
                    cache.entry(k).or_insert(freq)
                }
            };
            for (left, right) in res.iter_mut().zip(freq.iter()) {
                *left += right;
            }
        }
        res
    }
}

#[cfg(test)]
mod tests {
    use super::Puzzle;
    #[test]
    fn test_part1() {
        assert_eq!(Puzzle::new(TEST_INPUT).unwrap().part1(), 1588);
    }
    #[test]
    fn test_part2() {
        assert_eq!(Puzzle::new(TEST_INPUT).unwrap().part2(), 2188189693529);
    }
    static TEST_INPUT: &str = "NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C
";
}
